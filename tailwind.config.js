module.exports = {
  content: ['./app/**/*.php', './resources/**/*.{php,vue,js}'],
  theme: {
    screens: {
      'sm': '640px',
      'md': '768px',
      'lg': '1024px',
      'xl': '1280px',
      '2xl': '1440px',
    },
    container: {
      center: true,
      padding: {
        DEFAULT: '1.25rem',
        lg: '4.25rem'
      }
    },
    fontFamily: {
      'body': ['Gilroy', 'Serif', 'Arial'],
    },
    extend: {
      colors: {
        'slate': '#616161',
        'blue': '#F2F5F9',
        'black': '#004029',
        'green-100': '#648170',
        'green-200': '#EEFDF8',
        'green-300': '#12D47F',
        'green-400': '#004029'
      },
      spacing: {
        '7.5': '1.875rem',
        '18': '4.375rem',
        '22': '5.25rem',
        '52': '13.75rem',
      },
      letterSpacing: {
        'wide': '0.02em'
      },
      fontSize: {
        'sm': ['.875rem', '1.43'],
        'xl': ['1.25rem', '1.4'],
        '3xl': ['1.75rem', '.71'],
        '4xl': ['4rem', '1.44'],
        '6xl': ['4rem', '1.125']
      },
      borderRadius: {
        'md': '.3125rem',
        'xl': '.625rem'
      },
      boxShadow: {
        'header': '0px 16px 24px rgba(0, 0, 0, 0.04), 0px 0px 1px rgba(0, 0, 0, 0.04);'
      }
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
};